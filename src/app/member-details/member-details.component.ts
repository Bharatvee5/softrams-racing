import { Component, OnInit, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from '../app.service';
import { Router, ActivatedRoute } from '@angular/router';

// This interface may be useful in the times ahead...
interface Member {
  firstName: string;
  lastName: string;
  jobTitle: string;
  team: string;
  status: string;
}

@Component({
  selector: 'app-member-details',
  templateUrl: './member-details.component.html',
  styleUrls: ['./member-details.component.css']
})
export class MemberDetailsComponent implements OnInit, OnChanges {
  memberModel: Member;
  memberForm: FormGroup;
  submitted = false;
  alertType: String;
  alertMessage: String;
  teams = [];
  id;

  constructor(private fb: FormBuilder, private appService: AppService, private router: Router,
    private activatedRoute:ActivatedRoute) {
      this.id = this.activatedRoute.snapshot.paramMap.get('id');
    }

  ngOnInit() {
    this.loadUser();
    this.appService.getTeams().subscribe(teams => (this.teams = teams));
    this.memberModel = {
      firstName: '',
      lastName: '',
      jobTitle: '',
      team: '',
      status: ''
    }
  }

  loadUser() {
    this.appService.getMember(this.id).subscribe(member => (this.memberModel = member));
  }

  ngOnChanges() {}

  // TODO: Add member to members
  onSubmit(form: FormGroup) {
    this.appService.saveMember(this.id,this.memberModel).subscribe();
    this.router.navigate(['members']);
  }
}
