import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Router } from '@angular/router';
import { stringify } from '@angular/compiler/src/util';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {
  members = [];
  teams = [];
  newMember = {
    firstName: '',
    lastName: '',
    jobTitle: '',
    team: '',
    status: 'Active'
  };
  createFormStatus = false;


  constructor(public appService: AppService, private router: Router) {}

  ngOnInit() {
    this.updateData();
  }

  updateData() {
    this.appService.getMembers().subscribe(members => (this.members = members));
    this.appService.getTeams().subscribe(teams => (this.teams = teams));
  } 

  goToAddMemberForm() {
    console.log(`Hmmm...we didn't navigate anywhere`);
    this.createFormStatus = true; 
  }

  addUser() {
    if(this.newMember.firstName != "" && this.newMember.lastName != "" 
      && this.newMember.jobTitle != "" && this.newMember.team != "") {
        this.appService.addMember(this.newMember).subscribe();
        this.createFormStatus = false;
      }
      this.updateData();
  }

  editMemberByID(id: number) {
    this.createFormStatus = true;
  }

  deleteMemberById(id: number) {
    this.appService.deleteMember(id).subscribe();
    this.updateData();
  }
}
